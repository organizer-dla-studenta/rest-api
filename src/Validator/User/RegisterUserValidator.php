<?php


namespace App\Validator\User;


use App\Validator\ValidatorAbstract;
use Symfony\Component\Validator\Constraints as Assert;

class RegisterUserValidator extends ValidatorAbstract
{
    /**
     * @param array $array
     * @return array
     */
    public function validate(array $array): array
    {
        $constraint = new Assert\Collection([
            'email' =>[
                new Assert\NotBlank(),
                new Assert\Type('string'),
            ],
            'password' => [
            new Assert\NotBlank(),
                    new Assert\Type('string'),
                ],
        ]);
        return $this->validateData($array, $constraint);

    }
}