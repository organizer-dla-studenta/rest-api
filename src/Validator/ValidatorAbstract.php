<?php


namespace App\Validator;


use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\GroupSequence;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface as SymfonyValidatorInterface;

abstract class ValidatorAbstract
{
    private SymfonyValidatorInterface $validator;

    public function __construct(SymfonyValidatorInterface $validation)
    {
        $this->validator = $validation;
    }
    /**
     * @param ConstraintViolationListInterface $validate
     * @return array
     */
    protected function getErrorsFromConstraints(ConstraintViolationListInterface $validate): array
    {
        $result = [];

        /** @var ConstraintViolationInterface $item */
        foreach($validate as $item) {
            $result[$item->getPropertyPath() ?: $item->getInvalidValue()] = $item->getMessage();
        }
        return $result;
    }

    /**
     * @param mixed $data
     * @param Constraint|Constraint[] $constraints
     * @param string|GroupSequence|string[]|GroupSequence[]|null $groups
     * @return array
     */
    protected function validateData($data, $constraints, $groups = null): array
    {
        return $this->getErrorsFromConstraints($this->validator->validate($data, $constraints, $groups));
    }
}