<?php

namespace App\Validator\Event;

use App\Validator\ValidatorAbstract;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class EventValidator extends ValidatorAbstract
{
    /**
     * @param array $array
     * @return array
     */
    public function validate(array $array): array
    {
//        $callbackDateFrom = function($object, ExecutionContextInterface $context, $payload) use ($array) {
//            if(new \DateTime($object) <= new \DateTime()) {
//                $context->addViolation('Data startowa powinna być większa od daty poczatkowej');
//            }
//        };
        $callbackDatTo = function($object, ExecutionContextInterface $context, $payload) use ($array) {
            if($array['date_from'] >= $object) {
                $context->addViolation('Data końcowa powinna być większa od daty poczatkowej');
            }
        };
        $constraint = new Assert\Collection([
            'name' => [
                new Assert\NotBlank(),
                new Assert\Type('string'),
            ],
            'date_from' => [
                new Assert\NotBlank(),
                new Assert\Type('string'),
                //new Assert\Callback($callbackDateFrom)
            ],
            'date_to' => [
                new Assert\NotBlank(),
                new Assert\Type('string'),
                new Assert\Callback($callbackDatTo)
            ],
            'notification' => [
                new Assert\Required(),
                new Assert\Type('bool'),
            ],
            'description' => [
                new Assert\Optional([
                    new Assert\Type('string'),
                ]),
            ],
        ]);
        return $this->validateData($array, $constraint);
    }
}