<?php


namespace App\Transformer;


use Symfony\Component\Config\Definition\Exception\Exception;

abstract class TransformerAbstract
{
    /**
     * @param mixed $data
     * @param string|null $to
     * @param array $context
     * @return mixed
     * @throws Exception
     */
    public function transform($data, string $to = null, array $context = [])
    {
        throw new Exception('You must implement transform method');
    }

}