<?php

namespace App\Transformer;

use App\Entity\Event;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;

class EventTransformer extends TransformerAbstract
{

    public function transform($data, string $to = null, array $context = []): Event
    {
        $entity = $context[AbstractObjectNormalizer::OBJECT_TO_POPULATE] ?? new Event();

        if(isset($data['name'])){
            $entity->setName($data['name']);
        }
        if(isset($data['date_from'])){
            $entity->setDateFrom(new \DateTime($data['date_from']));
        }
        if(isset($data['date_to'])){
            $entity->setDateTo(new \DateTime($data['date_to']));
        }
        if(isset($data['description'])){
            $entity->setDescription($data['description']);
        }
        if(isset($data['notification'])){
            $entity->setNotification($data['notification']);
        }

        return $entity;
    }
}