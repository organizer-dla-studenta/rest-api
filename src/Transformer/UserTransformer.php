<?php


namespace App\Transformer;


use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;

class UserTransformer extends TransformerAbstract
{

    public function transform($data, string $to = null, array $context = []): User
    {
        $entity = $context[AbstractObjectNormalizer::OBJECT_TO_POPULATE] ?? new User();

        if(isset($data['email'])){
            $entity->setEmail($data['email']);
        }
        if(isset($data['password'])){
            $entity->setPassword($data['password']);
        }


        return $entity;
    }
}