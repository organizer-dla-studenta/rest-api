<?php


namespace App\Controller\Api;

use App\Transformer\UserTransformer;
use App\Validator\User\RegisterUserValidator;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController
{
    /**
     * @Rest\Post("/api/register", name="register_user")
     * @param Request $request
     * @param UserTransformer $userTransformer
     * @param RegisterUserValidator $validator
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param EntityManagerInterface $entityManager
     * @return Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function register(Request $request, UserTransformer $userTransformer, RegisterUserValidator $validator, UserPasswordEncoderInterface $passwordEncoder, EntityManagerInterface $entityManager)
    {
        $data = $request->request->all();
        $errors = $validator->validate($data);
        if (!empty($errors)) {
            $content = [
                'message' => 'Unprocessable Entity',
                'data' => $errors
            ];
            return new Response(
                json_encode($content), 422);
        }
        $user = $userTransformer->transform($data);
        $user->setRoles(["ROLE_USER"]);
        $user->setPassword($passwordEncoder->encodePassword($user, $user->getPassword()));
        $entityManager->persist($user);
        $entityManager->flush();
        return new Response();
    }


}