<?php
declare(strict_types=1);

namespace App\Controller\Api;

use App\Entity\Event;
use App\Repository\EventRepository;
use App\Service\NotificationService;
use App\Transformer\EventTransformer;
use App\Validator\Event\EventValidator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\NotifierInterface;
use Symfony\Component\Notifier\Recipient\Recipient;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;

/**
 * Class Event
 * @package App\Controller\Api
 * @Rest\Route("/api/v1/event", name="event_")
 */
class EventController
{

    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;
    /**
     * @var ContainerInterface
     */
    private ContainerInterface $container;
    /**
     * @var Security
     */
    private Security $security;
    /**
     * @var EventRepository
     */
    private EventRepository $eventRepository;
    /**
     * @var NotifierInterface
     */
    private NotifierInterface $notifier;


    public function __construct(EntityManagerInterface $entityManager,
                                ContainerInterface $container,
                                Security $security,
                                EventRepository $eventRepository)
    {
        $this->entityManager = $entityManager;
        $this->container = $container;
        $this->security = $security;
        $this->eventRepository = $eventRepository;
    }

    /**
     * @Rest\Get("/list", name="list")
     * @param EventRepository $eventRepository
     * @return Response
     */
    public function list(EventRepository $eventRepository)
    {

        $serializer = $this->container->get('jms_serializer');
        return new Response($serializer->serialize([
            'data'=> $eventRepository->findBy(['createdBy' => $this->security->getUser(),'deleted'=>false]),
            'count' => count($eventRepository->findBy(['createdBy' => $this->security->getUser(),'deleted'=>false]))
        ],  'json'));
    }

    /**
     * @Rest\Post("", name="post")
     * @param Request $request
     * @param EventValidator $validator
     * @param EventTransformer $transformer
     * @return Response
     */
    public function post(Request $request,EventValidator $validator, EventTransformer $transformer,NotificationService $notificationService)
    {
        $data = $request->request->all();
        $errors = $validator->validate($data);
        if (!empty($errors)) {
            $content = [
                'message' => 'Unprocessable Entity',
                'data' => $errors
            ];
            return new Response(
                json_encode($content), 422);
        }
        $event = $transformer->transform($data);
        $event->setCreatedBy($this->security->getUser());
        $this->entityManager->persist($event);
        $this->entityManager->flush();

        if ($event->getNotification()) {
            $notification =$notificationService->createNotificationByImportance(
                'Wydarzenie '.$event->getName(),
                'Dodano wydarzenie('.$event->getDateFrom()->format('Y-m-d H:i:s').'-'.$event->getDateTo()->format('Y-m-d H:i:s').')',
                Notification::IMPORTANCE_HIGH
            );
            $recipient = $notificationService->createAdminRecipient($event->getCreatedBy()->getEmail());
            $notificationService->sendNotification($notification, $recipient);

        }


        $serializer = $this->container->get('jms_serializer');
        return new Response($serializer->serialize($data,  'json'));
    }

    /**
     * @Rest\Put("/{id}", name="put")
     * @param Event $event
     * @param Request $request
     * @param EventValidator $validator
     * @param EventTransformer $transformer
     * @param NotificationService $notificationService
     * @return Response
     */
    public function put(Event $event,Request $request,EventValidator $validator, EventTransformer $transformer,NotificationService $notificationService)
    {
        $data = $request->request->all();
        $errors = $validator->validate($data);
        if (!empty($errors)) {
            $content = [
                'message' => 'Unprocessable Entity',
                'data' => $errors
            ];
            return new Response(
                json_encode($content), 422);
        }
        $transformer->transform($data,null,[AbstractObjectNormalizer::OBJECT_TO_POPULATE => $event]);
        $event->setUpdatedBy($this->security->getUser());
        $this->entityManager->flush();

        if ($event->getNotification()) {
            $notification =$notificationService->createNotificationByImportance(
                'Wydarzenie '.$event->getName(),
                'Zaktualizowano wydarzenie('.$event->getDateFrom()->format('Y-m-d H:i:s').'-'.$event->getDateTo()->format('Y-m-d H:i:s').')',
                Notification::IMPORTANCE_HIGH
            );
            $recipient = $notificationService->createAdminRecipient($event->getCreatedBy()->getEmail());
            $notificationService->sendNotification($notification, $recipient);
        }

        $serializer = $this->container->get('jms_serializer');
        return new Response($serializer->serialize($data,  'json'));
    }

    /**
     * @Rest\Get("/{id}", name="get")
     * @param Event $event
     * @return Response
     */
    public function get(Event $event)
    {
        $serializer = $this->container->get('jms_serializer');
        return new Response($serializer->serialize($event,  'json'));
    }


    /**
     * @Rest\Delete("/{id}", name="delete")
     * @param Event $event
     * @return Response
     */
    public function delete(Event $event,NotificationService $notificationService)
    {
        $event->setDeleted(true);
        $this->entityManager->flush();

        if ($event->getNotification()) {
            $notification =$notificationService->createNotificationByImportance(
                'Wydarzenie '.$event->getName(),
                'Usunięto wydarzenie('.$event->getDateFrom()->format('Y-m-d H:i:s').'-'.$event->getDateTo()->format('Y-m-d H:i:s').')',
                Notification::IMPORTANCE_HIGH
            );
            $recipient = $notificationService->createAdminRecipient($event->getCreatedBy()->getEmail());
            $notificationService->sendNotification($notification, $recipient);
        }

        $serializer = $this->container->get('jms_serializer');
        return new Response('',204);
    }
}