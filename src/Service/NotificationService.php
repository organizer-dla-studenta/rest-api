<?php


namespace App\Service;


use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\NotifierInterface;
use Symfony\Component\Notifier\Recipient\AdminRecipient;

class NotificationService
{
    /**
     * @var NotifierInterface
     */
    private NotifierInterface $notifier;

    /**
     * NotificationService constructor.
     * @param NotifierInterface $notifier
     */
    public function __construct(NotifierInterface $notifier)
    {
        $this->notifier = $notifier;
    }

    public function createNotificationByImportance(string $subject, string $content, string $importance){
        $notification = (new Notification($subject));
        $notification->content($content)
            ->importance($importance);
        return $notification;
    }

    public function createNotificationByChannels(string $subject, string $content, array $channels){
        $notification = (new Notification($subject, $channels));
        $notification->content($content);
        return $notification;
    }

    public function createAdminRecipient(string $email='', string $phone=''){
        return new AdminRecipient($email,$phone);
    }
    public function sendNotification(Notification $notification, AdminRecipient $adminRecipient){
        $this->notifier->send($notification, $adminRecipient);
    }
}